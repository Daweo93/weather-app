import axios, { AxiosPromise } from 'axios';
import { IWeather } from '../types';
import { config } from '../../../config';

const getCurrentWeather = (city: string): AxiosPromise<IWeather> =>
  axios.get(config.API_URL + 'weather', {
    params: {
      q: city,
      APPID: config.APPID,
      units: 'metric'
    }
  });

const getFiveDaysWeather = (city: string) =>
  axios.get(config.API_URL + 'forecast/daily', {
    params: {
      q: city,
      units: 'metric',
      cnt: '7',
      APPID: config.APPID
    }
  });

export { getCurrentWeather, getFiveDaysWeather };
