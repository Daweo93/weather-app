const merge = require('webpack-merge');
const dev = require('./webpack.dev.js');
const path = require('path');

module.exports = merge(dev, {
  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  }
});
