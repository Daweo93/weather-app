# Webpack Quick Starter

Make it beautiful and write your styles in Sass.  
Handle user actions with the latest Javascript features.  
Make sure your JS is working as intended and test it with Jest.


#Config
Use env.example to create .env file which is required to run this application. 
> **API_URL**: `string` - URL to https://openweathermap.org/api endpoint

> **APP_ID**: `string` - OpenWeatherMap API key


## Usage
Install dependencies using `yarn` or `npm`

```
yarn OR npm i
```

Run with dev server
```
yarn start OR npm start
```

Run without dev server
```
yarn dev OR npm run dev
```

Build production version
```
yarn build OR npm run build
```

Test Javscript using Jest
```
yarn test OR npm run test
```
